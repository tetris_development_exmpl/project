<?php

namespace app\system;

use League\Flysystem\Config;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\FilesystemException;
use League\Flysystem\InvalidVisibilityProvided;
use League\Flysystem\UnableToCheckExistence;
use League\Flysystem\UnableToCopyFile;
use League\Flysystem\UnableToCreateDirectory;
use League\Flysystem\UnableToDeleteDirectory;
use League\Flysystem\UnableToDeleteFile;
use League\Flysystem\UnableToMoveFile;
use League\Flysystem\UnableToReadFile;
use League\Flysystem\UnableToRetrieveMetadata;
use League\Flysystem\UnableToWriteFile;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\UnknownMethodException;

/**
 * @
 */
class Filesystem extends Component
{
    public string $adapter;
    public array $adapterConfig;
    protected \League\Flysystem\Filesystem $filesystem;
    
    public function init(): void
    {
        
        $class = $this->adapter;
        $adapterInstance = new $class(...$this->adapterConfig);
        
        $this->filesystem = new \League\Flysystem\Filesystem($adapterInstance);
        
        parent::init();
    }
    
    public function __call($name, $params)
    {
        if (method_exists($this->filesystem, $name)) {
            return call_user_func_array([$this->filesystem, $name], $params);
        }
        throw new UnknownMethodException('Calling unknown method: ' . get_class($this) . "::$name()");
    }
}