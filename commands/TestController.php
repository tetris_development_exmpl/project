<?php

namespace app\commands;

use League\Flysystem\Filesystem;
use yii\console\Controller;

class TestController extends Controller
{
    public function actionTest(){
        $filesystem = \Yii::$app->get('filesystem');
        /** @var Filesystem $filesystem */
        var_dump($filesystem->listContents('/'));
    }
}